# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include blender::install
class blender::install {
  package { $blender::package_name:
    ensure => $blender::package_ensure,
  }
}
